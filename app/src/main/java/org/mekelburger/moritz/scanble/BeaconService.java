package org.mekelburger.moritz.scanble;

import android.app.Service;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Binder;
import android.os.IBinder;
import android.util.Log;

public class BeaconService extends Service {
    private final static String TAG = "BeaconService";

    private IBinder mBinder = new LocalBinder();

    public class LocalBinder extends Binder {
        public BeaconService getServerInstance() {
            return BeaconService.this;
        }
    }

    public BeaconHandler getBeaconHandler() {
        return beaconHandler;
    }

    private BeaconHandler beaconHandler;
    private Thread beaconHandlerThread;

    public BeaconService() {

    }

    @Override
    public IBinder onBind(Intent intent) {
        return this.mBinder;
    }

    @Override
    public void onCreate() {
        Log.v(TAG, "onCreate");
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);

        this.beaconHandler = new BeaconHandler(bluetoothManager);
        this.beaconHandlerThread = new Thread(beaconHandler);
        this.beaconHandlerThread.start();
    }

    @Override
    public void onDestroy() {
        Log.v(TAG, "onDestroy");
        try {
            this.beaconHandlerThread.join(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
