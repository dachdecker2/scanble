package org.mekelburger.moritz.scanble;

import android.bluetooth.BluetoothDevice;
import android.util.Log;

/**
 * Created by moritz on 08.11.2014.
 */
public class Beacon extends SimpleBeacon{
    private String mac = new String();
    private AdvertisementHistory advertisements = new AdvertisementHistory();
    private long last_advertisement_ts = 0;

    private static final String TAG = "Beacon";
    private int tx;

    private final static float C = -(float) Math.log(10) / 20;

    public Beacon(BluetoothDevice device, byte[] scanRecord, float[] position) {
        super(position, 0, 0, 0);
        this.mac = device.getAddress();
        this.tx = scanRecord[29];
    }

    public SimpleBeacon flatCopy() {
        SimpleBeacon result = new SimpleBeacon(this.getPosition().clone(), this.getDistance(),
                this.getDistanceError(), this.advertisements.getCurrentValue(this.getMac()));
        return result;
    }

    public void addRssi(int rssi) {
        this.last_advertisement_ts = System.currentTimeMillis();
        this.advertisements.addAdvertisement(rssi);
    }

    public float getDistance() {
        float ratio_power = this.tx - this.advertisements.getCurrentValue(this.getMac());
        float distance = (float) Math.pow(Math.pow(10., ratio_power / 10.), 0.5);
        Log.d(TAG, String.format("Distance %s: %.2f (from %d measurements)", this.getMac(),
                distance, this.advertisements.size()));
        return distance;
    }

    public float getDistanceError() {
        float distanceError = Beacon.C * this.getDistance() * this.advertisements.getError();
        Log.d(TAG, String.format("Distance error %s: %.2f", this.getMac(), distanceError));
        Log.v(TAG, String.format("Distance error %s from %s", this.getMac(), this.advertisements.toString()));
        return distanceError;
    }

    public String getMac() {
        return this.mac;
    }
}
