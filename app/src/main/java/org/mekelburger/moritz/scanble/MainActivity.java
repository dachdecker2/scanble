package org.mekelburger.moritz.scanble;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RelativeLayout;
import android.widget.Switch;
import android.widget.TextView;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;


public class MainActivity extends Activity {
    private final static String TAG = "MainActivity";

    private boolean bounded;
    private BluetoothAdapter bluetoothAdapter;
    BeaconService beaconService;

    private static final ScheduledExecutorService requestPosition =
            Executors.newSingleThreadScheduledExecutor();
    private ScheduledFuture<?> requestPositionFuture;
    private Handler runhandler = new Handler();
    private Boolean doAutoUpdate = false;


    ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName name, IBinder service) {
            Log.d(TAG, "Connected to BeaconService");
            bounded = true;
            BeaconService.LocalBinder localBinder = (BeaconService.LocalBinder) service;
            beaconService = localBinder.getServerInstance();
        }

        @Override
        public void onServiceDisconnected(ComponentName name) {
            Log.d(TAG, "Disconnected from BeaconService");
            bounded = false;
            beaconService = null;
        }
    };


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        this.bluetoothAdapter = bluetoothManager.getAdapter();
        if (this.bluetoothAdapter == null || !this.bluetoothAdapter.isEnabled()) {
            Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableBtIntent, 1);
        }
        Intent intent = new Intent(this, BeaconService.class);
        startService(intent);
        Button do_button = (Button) findViewById(R.id.button);
        do_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(TAG, "Button pressed - START");
                updateText("Button");
                Log.d(TAG, "Button pressed - END");
            }
        });

        Switch do_switch = (Switch) findViewById(R.id.myswitch);
        do_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked)
                {
                    doAutoUpdate = true;
                    runhandler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            Log.d(TAG, "Timer2 - START");
                            updateText("Scheduler");
                            Log.d(TAG, "Timer2 - END");
                            if (doAutoUpdate)
                                runhandler.postDelayed(this, 1000);
                        }
                    }
                    , 1000);
                }
                else
                    doAutoUpdate = false;
            }
        });

        bindService(intent, this.serviceConnection, BIND_ABOVE_CLIENT);

        getWindow().getDecorView().findViewById(android.R.id.content).invalidate();
        ((TextView) findViewById(R.id.textView)).invalidate();
    }

    private void updateText (String defaultString) {
        String beaconString = "";
        for (SimpleBeacon b: beaconService.getBeaconHandler().getTmpBeacons()) {
            beaconString += b.toString();
            beaconString += "\n";
        }

        ((TextView) findViewById(R.id.textView)).setText(
                String.format("caller: %s\nPosition: %.2f, %.2f\n%s",
                        defaultString,
                        beaconService.getBeaconHandler().getPos()[0],
                        beaconService.getBeaconHandler().getPos()[1],
                        beaconString));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onStop() {
        super.onStop();
//        this.requestPositionFuture.cancel(false);
        if (this.bounded) {
            unbindService(this.serviceConnection);
            bounded = false;
        }
    }
}
